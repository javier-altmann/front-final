import { Injectable } from "@angular/core";
import { ApiClient } from './api-client.service';
import { IUsuarioConResultadoResponse } from "../interfaces/IUsuarioConResultadoResponse";
import { IGrupoDelUsuario } from "../interfaces/IGrupoDelUsuario";
import { IEstablecimientoDestacado } from "../interfaces/IEstablecimientoDestacado";
import { Observable } from "rxjs/Observable";


@Injectable()
export class GruposService {
  private URL_ENDPOINT_USUARIOS_DEL_GRUPO = "Grupos/";
  private URL_ENDPOINT_GRUPOS = "Grupos";

  constructor(private http: ApiClient) { }

  requestGrupos(search: string, limit: number, offset: number, enableLoadingSpinner: boolean = true) {
    return this.http.get<IGrupoDelUsuario>(this.URL_ENDPOINT_USUARIOS_DEL_GRUPO + `Own?search=${search}&limit=${limit}&offset=${offset}`, enableLoadingSpinner);
  }

  post(data: any) {
    return this.http.post(this.URL_ENDPOINT_GRUPOS, JSON.stringify(data));
  }

  get(grupoId: number) {
    return this.http.get<IUsuarioConResultadoResponse>(this.URL_ENDPOINT_USUARIOS_DEL_GRUPO + grupoId + "/Usuarios");
  }

  getRecomendados(limit: number, offset: number, grupoId: number, enableLoadingSpinner?: boolean): Observable<IEstablecimientoDestacado> {
    return this.http.get<IEstablecimientoDestacado>(this.URL_ENDPOINT_GRUPOS + `/${grupoId}/Recomendados?limit=${limit}&offset=${offset}`, enableLoadingSpinner);
  }

}