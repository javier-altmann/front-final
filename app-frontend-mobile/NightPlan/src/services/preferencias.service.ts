import { Injectable } from "@angular/core";
import { ApiClient } from './api-client.service';
import { Observable } from "rxjs/Observable";
import { IOpcionesPreferencias } from "../interfaces/IOpcionesPreferencias";
import { IPreferencias } from "../interfaces/IPreferencias";

@Injectable()
export class PreferenciasService {
  private URL_ENDPOINT_CARACTERISTICAS: string = "Caracteristicas";
  private URL_ENDPOINT_BARRIOS: string = "Barrios";
  private URL_ENDPOINT_GASTRONOMIA: string = "Gastronomia";
  private URL_ENDPOINT_PREFERENCIAS: string = "Preferencias";

  constructor(private http: ApiClient) { }

  getListaDeGastronomia(): Observable<IOpcionesPreferencias[]> {
    return this.http.get<IOpcionesPreferencias[]>(this.URL_ENDPOINT_GASTRONOMIA);
  }

  getListaDeBarrios(): Observable<IOpcionesPreferencias[]> {
    return this.http.get<IOpcionesPreferencias[]>(this.URL_ENDPOINT_BARRIOS);
  }

  getListaDeCaracteristicas(): Observable<IOpcionesPreferencias[]> {
    return this.http.get<IOpcionesPreferencias[]>(this.URL_ENDPOINT_CARACTERISTICAS);
  }

  post(preferencias: IPreferencias) {
    return this.http.post(this.URL_ENDPOINT_PREFERENCIAS, JSON.stringify(preferencias));
  }
}