import { Injectable, Injector } from "@angular/core";
import { ApiClient } from './api-client.service';
import { ILoginResult } from "../interfaces/ILoginResult";
import { IUsuarioLogin } from "../interfaces/IUsuarioLogin";
import { IFacebook } from "../interfaces/IFacebook";

@Injectable()
export class LoginService {
  get URL_ENDPOINT_LOGIN(): string {
    return 'Authentication';
  }

  get URL_ENDPOINT_LOGIN_FACEBOOK(): string {
    return 'Prueba';
  }

  constructor(private http: ApiClient, protected injector: Injector) { }

  post(usuario: IUsuarioLogin) {
    return this.http.post<ILoginResult>(this.URL_ENDPOINT_LOGIN, JSON.stringify(usuario))
  }

  postFacebook(fb: IFacebook) {
    return this.http.post<ILoginResult>(this.URL_ENDPOINT_LOGIN_FACEBOOK, JSON.stringify(fb))
  }

  logout() {
    localStorage.removeItem('token');
  }

  login(loginResult: ILoginResult) {
    localStorage.setItem('token', loginResult.token);
    localStorage.setItem('id', loginResult.id);
  }

  get isLoggedIn(): boolean {
    return !!localStorage.getItem('token');
  }
 
}