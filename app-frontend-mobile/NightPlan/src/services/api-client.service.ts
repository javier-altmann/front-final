import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { ConfigService } from "./config.service";
import { Observable } from "rxjs/Observable";
import { share } from 'rxjs/operators';
import { LoadingController } from "ionic-angular";
import { ToastService } from "./toast.service";

@Injectable()
export class ApiClient {
  constructor(
    private httpClient: HttpClient,
    private configService: ConfigService,
    private loadingCtrl: LoadingController,
    private toast: ToastService) { }

  get headers() {
    let token = localStorage.getItem('token');
    let seed = { 'Content-Type': 'application/json; charset=utf-8' };
    seed['Access-Control-Allow-Origin'] = '*';
    seed['Accept'] = 'application/json';
    
    if (!!token) {
      seed['Authorization'] = 'Bearer ' + token;
    }
    return new HttpHeaders(seed)
  }

  private requestOptions(data: any) {
    return {
      headers: this.headers,
      body: data,
    };
  }

  private doRequest<T>(url: string, method?: string, data?: any, enableLoadingSpinner: boolean = true) {
    let loading = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    if (enableLoadingSpinner) loading.present();
    let observable = this.httpClient.request<T>(
      !!method ? method : 'GET',
      this.configService.baseUrl + url,
      this.requestOptions(data)).pipe(share());
    observable.subscribe(null, error => {
      let response = error as HttpResponse<T>;
      if (response.status === 401) {
        localStorage.removeItem('token');
        this.loadingDimiss(enableLoadingSpinner, loading);
        this.toast.show("El usuario o contraseña es incorrecto");
    }else if(response.status == 404){
      this.loadingDimiss(enableLoadingSpinner, loading);      
    }
    }, () => {
      this.loadingDimiss(enableLoadingSpinner, loading);
    });
    return observable;
  }

  get<T>(url: string, enableLoadingSpinner: boolean = true): Observable<T> {
    return this.doRequest<T>(url, null, null, enableLoadingSpinner);
  }

  post<T>(url: string, body: any): Observable<T> {
    return this.doRequest<T>(url, 'POST', body);
  }

  put<T>(url: string, body: any): Observable<T> {
    return this.doRequest<T>(url, 'PUT', body);
  }

  loadingDimiss(enableLoadingSpinner: boolean, loading){
    loading.dismiss();
  }
}