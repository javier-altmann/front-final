import { Injectable } from "@angular/core";
import { ApiClient } from './api-client.service';
import { IEstablecimientoDestacado } from "../interfaces/IEstablecimientoDestacado";
import { IEstablecimientoConDetalle } from "../interfaces/IEstablecimientoConDetalle";

@Injectable()
export class EstablecimientosService {
    get URL_ENDPOINT_ESTABLECIMIENTOS(): string {
        return 'Establecimientos';
    }

    constructor(private http: ApiClient) { }

    listDestacados(limit: number, offset: number, enableLoadingSpinner: boolean = true) {
        return this.http.get<IEstablecimientoDestacado>(`${this.URL_ENDPOINT_ESTABLECIMIENTOS}?limit=${limit}&offset=${offset}`, enableLoadingSpinner);
    }

    get(id: number){
        return this.http.get<IEstablecimientoConDetalle>(this.URL_ENDPOINT_ESTABLECIMIENTOS + `/${id}`);    
    }
}