import { Injectable } from "@angular/core";
import { ApiClient } from './api-client.service';
import { UsuariosVm } from "../interfaces/usuariosVm";
import { Observable } from "rxjs/Observable";
import { IUsuarioRegistrado } from "../interfaces/IUsuarioRegistrado";

@Injectable()
export class UsuariosService {
  get URL_ENDPOINT_USUARIOS(): string {
    return 'Usuarios';
  }

  constructor(private http: ApiClient) { }

  Query(text: string, showLoading: boolean = false) {
    return this.http.get<UsuariosVm[]>(
      this.URL_ENDPOINT_USUARIOS + "?query=" + text,
      showLoading);
  }

  put(usuario: IUsuarioRegistrado) {
    return this.http.put<IUsuarioRegistrado>(this.URL_ENDPOINT_USUARIOS + '/Own', JSON.stringify(usuario));
  }

  get(): Observable<IUsuarioRegistrado> {
    return this.http.get<IUsuarioRegistrado>(this.URL_ENDPOINT_USUARIOS + '/Own');
  }

  get user(): string {
    return localStorage.getItem('fullName');
  }

  saveFullName() {
    let fullName = "";
    this.get().subscribe(res => {
      fullName = res.nombre + " " + res.apellido;
      localStorage.setItem('fullName', fullName);
    });
  }

  updateFullName(nombre: string, apellido: string) {
    let fullName = nombre + " " + apellido;
    localStorage.removeItem('fullName');
    localStorage.setItem('fullName', fullName);
  }

  getUserId(): number {
    return parseInt(localStorage.getItem('id'));
  }

  get fullName() {
    return localStorage.getItem('fullName');
  }
}