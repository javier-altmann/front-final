import { Injectable } from "@angular/core";
import { ApiClient } from './api-client.service';
import { IUsuarioRegistrado } from "../interfaces/IUsuarioRegistrado";
import { ILoginResult } from "../interfaces/ILoginResult";


@Injectable()
export class RegistrarseService {
  get URL_ENDPOINT_REGISTRARSE(): string {
    return 'Usuarios';
  }

  constructor(private http: ApiClient) { }

  post(usuario: IUsuarioRegistrado) {
    return this.http.post<ILoginResult>(this.URL_ENDPOINT_REGISTRARSE, JSON.stringify(usuario));
  }
}
