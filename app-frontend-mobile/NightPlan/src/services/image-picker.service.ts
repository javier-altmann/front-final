import { Injectable } from "@angular/core";
import { Camera } from "@ionic-native/camera";
import { ActionSheetController, Platform, LoadingController, Loading } from "ionic-angular";

const imageHeader: string = 'data:image/png;base64,';

export class ImageResource {
  constructor(private imageUri: string, private thumbnailUri: string) { }

  get ImageSrc() {
    return imageHeader + this.imageUri;
  }

  get ThumbnailSrc() {
    return this.thumbnailUri;
  }
}

@Injectable()
export class ImagePickerService {

  constructor(
    private camera: Camera,
    public platform: Platform,
    public actionsheetCtrl: ActionSheetController,
    private loadingCtrl: LoadingController) { }

  loading: Loading;

  private presentLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    this.loading.present();
  }

  Pick(callback: (x: (ImageResource)) => void) {
    let actionSheet = this.actionsheetCtrl.create({
      title: 'Cargar imagen',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Tomar foto',
          icon: 'md-camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA, callback);
            this.presentLoading();
          }
        },
        {
          text: 'Galeria',
          icon: 'md-images',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY, callback);
            this.presentLoading();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: 'close',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  imageResize(imageURI: string, width: number): Promise<any> {
    return new Promise((resolve) => {
      var img = window.document.createElement('img');
      img.onload = () => resolve(this.imageToUri(img, width, img.naturalHeight / img.naturalWidth * width));
      img.src = imageHeader + imageURI;
    });
  }

  imageToUri(img: HTMLImageElement, width: number, height: number) {
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    canvas.width = width;
    canvas.height = height;
    ctx.drawImage(img, 0, 0, width, height);
    return canvas.toDataURL();
  }

  takePicture(sourceType, callback) {
    const options = {
      destinationType: 0,
      sourceType: sourceType,
      encodingType: 1,
      mediaType: 0,
      targetHeight: 720,
    }

    this.camera.getPicture(options).then(imageURI => {
      this.imageResize(imageURI, 100).then(thumbnailURI => {
        callback(new ImageResource(imageURI, thumbnailURI));
        this.loading.dismiss();
      });
    }, rejected => this.loading.dismiss());
  }
}
