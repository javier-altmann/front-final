import { Injectable } from "@angular/core";
import { ApiClient } from './api-client.service';
import { IVotacion } from "../interfaces/IVotacion";


@Injectable()
export class VotacionService {
  get URL_ENDPOINT_VOTACION(): string {
    return 'Votacion';
  }

  get URL_ENDPOINT_ESTABLECIMIENTOS(): string {
    return 'Establecimientos';
  }

  constructor(private http: ApiClient) { }

  post(votacion: IVotacion) {
    return this.http.post(this.URL_ENDPOINT_VOTACION, JSON.stringify(votacion));
  }

  get(id: number){
    return this.http.get(this.URL_ENDPOINT_VOTACION+"/"+`${id}`);
  }

  getEstablecimientoName(id: number){
    return this.http.get(this.URL_ENDPOINT_ESTABLECIMIENTOS +"/" + `${id}`);
  }
}