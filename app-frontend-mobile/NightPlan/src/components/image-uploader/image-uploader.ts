import { Component, Output, EventEmitter, Input } from '@angular/core';
import { ImagePickerService, ImageResource } from '../../services/image-picker.service';
import { faCamera } from '@fortawesome/free-solid-svg-icons';

/**
 * Generated class for the ImageUploadComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'image-uploader',
  templateUrl: 'image-uploader.html'
})
export class ImageUploaderComponent {
  @Input() initialPicture: string = '';
  @Output() imageSelected = new EventEmitter<ImageResource>();

  selectedPicture: ImageResource;
  faCamera = faCamera;

  constructor(private imagePicker: ImagePickerService) {}

  get thumbnail() :string {
    return this.selectedPicture ? this.selectedPicture.ThumbnailSrc : this.initialPicture;
  }

  addPicture () {
    this.imagePicker.Pick(result => {
      this.selectedPicture = result;
      this.imageSelected.emit(this.selectedPicture);
    });
  }
}