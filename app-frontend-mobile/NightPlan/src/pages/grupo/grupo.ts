import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GruposService } from '../../services/grupos.service';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { IUsuarioConResultado } from '../../interfaces/IUsuarioConResultado';
import { IUsuarioConResultadoResponse } from '../../interfaces/IUsuarioConResultadoResponse';
import { PreferenciasPage } from '../preferencias/preferencias';
import { IEstablecimiento } from '../../interfaces/IEstablecimiento';
import { IEstablecimientoDestacado } from '../../interfaces/IEstablecimientoDestacado';
import { DetalleEstablecimientoPage } from '../detalle-establecimiento/detalle-establecimiento';
import { ToastService } from '../../services/toast.service';
import { VotacionService } from '../../services/votacion.service';
import { IData } from '../../interfaces/IData';
import { IResponse} from '../../interfaces/IResponse';


@Component({
  selector: 'page-grupos',
  templateUrl: 'grupo.html'
})
export class GrupoPage {
  respuestaLoguedUser: boolean;
  respuestas: IUsuarioConResultado[] = [];
  grupoId: number;
  faCheck = faCheck;
  recomendados: IEstablecimiento[];
  resultado: any;

  constructor(
    public navCtrl: NavController,
    private grupo: GruposService,
    navParams: NavParams,
    private toast: ToastService,
    private votacion: VotacionService) {
    this.grupoId = navParams.get('grupoId');
  }

  ionViewWillEnter() {
    this.getUsuariosDelGrupoSubscribe();
    this.getResultadoVotacion();
  }

  private get todosRespondieron(): boolean {
    return this.respuestaLoguedUser && this.respuestas.every(x => x.resultado);
  }

  private getUsuariosDelGrupoSubscribe() {
    this.grupo.get(this.grupoId)
      .subscribe((usuarios: IUsuarioConResultadoResponse) => {
        this.respuestas = !!usuarios.respuestas ?
          usuarios.respuestas : [];
        this.respuestas = (this.respuestas = !!usuarios.respuestas ? usuarios.respuestas : []);
        this.respuestaLoguedUser = usuarios.respuestaLoguedUser.resultado;
        if (this.todosRespondieron) {
          this.getEstablecimientosRecomendados();
        }
      });
  }

  private getEstablecimientosRecomendados() {
    this.grupo.getRecomendados(1, 0, this.grupoId)
      .subscribe((recomendados: IEstablecimientoDestacado) => {
        this.recomendados = recomendados.data;
      });
  }
  private saveVoto(establecimientoId) {
    this.votacion.post({ IdEstablecimiento: establecimientoId, IdGrupo: this.grupoId})
    .subscribe((res: IResponse) => this.toast.show(res.mensajePersonalizado));
  }
  private async getResultadoVotacion(){
    this.resultado = []
    this.votacion.get(this.grupoId).subscribe((res: any) => {
      for (const establecimiento of res.data) {
        const dataEstablecimiento = this.votacion.getEstablecimientoName(establecimiento.idEstablecimiento).subscribe((res: any) => {
          const establecimientoMapped = Object.assign(establecimiento, { name: res.nombre })

          this.resultado.push(establecimientoMapped)
        })
      }
  });
   
  }
  goToPreferencias() {
    this.navCtrl.push(PreferenciasPage, { grupoId: this.grupoId });
  }
  
  goToDetalle(detalleId) {
    this.navCtrl.push(DetalleEstablecimientoPage, { detalleId: detalleId });
  }
}