import { Component } from '@angular/core';
import { NavController, InfiniteScroll } from 'ionic-angular';
import { IEstablecimiento } from '../../interfaces/IEstablecimiento';
import { DetalleEstablecimientoPage } from '../detalle-establecimiento/detalle-establecimiento';
import { EstablecimientosService } from '../../services/establecimientos.service';

@Component({
  selector: 'page-destacados',
  templateUrl: 'destacados.html'
})
export class DestacadosPage {
  establecimientos: IEstablecimiento[];
  limit: number = 6;
  offset: number = 0;
  fullName: string;
  constructor(public navCtrl: NavController, private establecimientosService: EstablecimientosService) { 
  }

  ionViewDidLoad() {
    this.establecimientosService.listDestacados(this.limit, this.offset)
      .subscribe(response => {
        this.establecimientos = response.data;
        this.offset += this.limit;
      });      
  }

  siguiente_pagina(infiniteScroll: InfiniteScroll) {
    this.establecimientosService.listDestacados(this.limit, this.offset, false)
      .subscribe(response => {
        this.establecimientos = this.establecimientos.concat(response.data);
        this.offset += this.limit;
        infiniteScroll.complete();
      });
  }

  goToDetalle(detalleId) {
    this.navCtrl.push(DetalleEstablecimientoPage, { detalleId: detalleId });
  }
}