import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { IUsuarioRegistrado } from '../../interfaces/IUsuarioRegistrado';
import { RegistrarseService } from '../../services/registrarse.service';
import { ImageResource } from '../../services/image-picker.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastService } from '../../services/toast.service';
import { LoginService } from '../../services/login.service';
import { UsuariosService } from '../../services/usuarios.service';

@Component({
  selector: 'page-registrarse',
  templateUrl: 'registrarse.html'
})
export class RegistrarsePage {
  form: FormGroup;
  imagen: string;
  constructor(public navCtrl: NavController,
    private registrarse: RegistrarseService,
    private toastService: ToastService,
    private loginService: LoginService,
    private usuariosService: UsuariosService,
    formBuilder: FormBuilder) {
    this.validacionesFormularioRegistrarse(formBuilder);
  }

  validacionesFormularioRegistrarse(formulario: FormBuilder) {
    this.form = formulario.group({
      nombre: ['', Validators.required],
      apellido: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      fechaNacimiento: ['', Validators.required],
      imagenPerfil: ''
    });
  }

  imageSelected(image: ImageResource) {
    this.imagen = image.ThumbnailSrc;
  }

  create() {
    let user = <IUsuarioRegistrado>this.form.value;
    user.imagenPerfil = this.imagen;
    this.registrarse.post(user)
      .subscribe(data => {
        this.loginService.login(data);
        this.toastService.show("Se registro exitosamente");
        this.usuariosService.saveFullName();
      }, error => {
        this.toastService.show(!!error ?
          error.error.Email[0] :
          'No se pudo registrar, intente nuevamente');
      });
  }
}
