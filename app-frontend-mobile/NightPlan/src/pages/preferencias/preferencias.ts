import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PreferenciasService } from '../../services/preferencias.service';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { IOpcionesPreferencias } from '../../interfaces/IOpcionesPreferencias';
import { IPreferenciasSelected } from '../../interfaces/IPreferenciasSelected';
import { ToastService } from '../../services/toast.service';
import { IPreferencias } from '../../interfaces/IPreferencias';
import { faCheck } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'page-preferencias',
  templateUrl: 'preferencias.html'
})
export class PreferenciasPage {
  barrios: IOpcionesPreferencias[] = [];
  gastronomias: IOpcionesPreferencias[] = [];
  caracteristicas: IOpcionesPreferencias[] = [];
  preferenciasSelected: IPreferenciasSelected = {
    IdsBarrios: [],
    IdsCaracteristicas: [],
    IdsGastronomia: [],
  };
  tabBarrios: string;
  tabCaracteristicas: string;
  tabGastronomia: string;
  preferencias: IPreferencias = {
    grupoId: 0,
    respuesta: {
      IdsCaracteristicas: [],
      IdsGastronomia: [],
      IdsBarrios: [],
    }
  };
  faCheck = faCheck;

  constructor(
    public navCtrl: NavController,
    private preferenciasService: PreferenciasService,
    private toastService: ToastService,
    private navParams: NavParams) {
    this.tabBarrios = "tabBarrios";
    this.preferencias.grupoId = this.navParams.get('grupoId');
  }

  ionViewDidLoad() {
    this.getItems();
  }

  getCheckedvalue() {
    this.preferenciasSelected.IdsBarrios = this.getSelectedIds(this.barrios);
    this.preferenciasSelected.IdsGastronomia = this.getSelectedIds(this.gastronomias);
    this.preferenciasSelected.IdsCaracteristicas = this.getSelectedIds(this.caracteristicas);
    return this.preferenciasSelected;
  }

  private getItems() {
    forkJoin(
      this.preferenciasService.getListaDeBarrios(),
      this.preferenciasService.getListaDeCaracteristicas(),
      this.preferenciasService.getListaDeGastronomia()
    )
      .subscribe(([barrios, caracteristicas, gastronomias]) => {
        this.barrios = barrios;
        this.caracteristicas = caracteristicas;
        this.gastronomias = gastronomias;
      });
  }

  private getSelectedIds(items: IOpcionesPreferencias[]): number[] {
    return items.filter(item => item.selected)
      .map(item => item.id);
  }

  savePreferenciasSelected() {
    this.preferencias.respuesta = this.getCheckedvalue();
    this.preferenciasService.post(this.preferencias)
      .subscribe(x => {
        this.toastService.show("Se guardaron correctamente las preferencias");
        this.navCtrl.pop();
      });
  }

  didUserChoosePreferences(): boolean {
    let prefrencesChecked = this.getCheckedvalue();
    if (prefrencesChecked.IdsBarrios.length > 0 && prefrencesChecked.IdsCaracteristicas.length > 0 && prefrencesChecked.IdsGastronomia.length > 0) {
      return false;
    }
    return true;
  }
}