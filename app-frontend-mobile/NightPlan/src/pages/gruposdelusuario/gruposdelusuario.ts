import { Component, ViewChild } from '@angular/core';
import { NavController, InfiniteScroll, Searchbar } from 'ionic-angular';
import { GruposService } from '../../services/grupos.service';
import { debounceTime } from 'rxjs/operators';
import { CrearGrupoPage } from '../crear-grupo/crear-grupo';
import { GrupoPage } from '../grupo/grupo';
import { IGrupo } from '../../interfaces/IGrupo';
import { IGrupoDelUsuario } from '../../interfaces/IGrupoDelUsuario';

@Component({
  selector: 'page-grupos-del-usuario',
  templateUrl: 'gruposdelusuario.html'
})
export class GruposDelUsuarioPage {
  grupos: IGrupo[];
  limit: number = 10;
  offset: number = 0;
  search = '';
  @ViewChild('searchbar') private searchbar: Searchbar;

  constructor(public navCtrl: NavController, private gruposService: GruposService) { }

  ionViewDidEnter() {
    this.searchbar.ionInput.asObservable()
      .pipe(debounceTime(300))
      .subscribe(this.onSearch.bind(this));

    this.obtenerGruposDelUsuario();
  }

  onScroll(infiniteScroll: InfiniteScroll) {
    this.gruposService.requestGrupos(this.search, this.limit, this.offset, false)
      .subscribe((response: IGrupoDelUsuario) => {
        this.grupos = this.grupos.concat(response.data);
        this.offset += this.limit;
        infiniteScroll.complete();
      });
  }

  onSearch({ target: { value: searchTerm } }) {
    this.search = searchTerm;
    this.obtenerGruposDelUsuario(searchTerm);
  }

  private obtenerGruposDelUsuario(searchTerm = '') {
    this.gruposService.requestGrupos(searchTerm, this.limit, 0)
      .subscribe((response: IGrupoDelUsuario) => {
        this.grupos = response.data;
        this.offset = this.limit;
      });
  }

  goToCrearGrupo() {
    this.navCtrl.push(CrearGrupoPage);
  }

  goToGrupo(grupoId) {
    this.navCtrl.push(GrupoPage, { grupoId: grupoId });
  }
}