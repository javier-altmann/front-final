import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UsuariosService } from '../../services/usuarios.service';
import { ToastService } from '../../services/toast.service';
import { ImageResource } from '../../services/image-picker.service';
import { IUsuarioRegistrado } from '../../interfaces/IUsuarioRegistrado';

@Component({
  selector: 'page-mi-cuenta',
  templateUrl: 'mi-cuenta.html',
})
export class MiCuentaPage {
  user: IUsuarioRegistrado;

  constructor(public navCtrl: NavController,
    private usuario: UsuariosService,
    private toast: ToastService) { }

  imageSelected(image: ImageResource) {
    this.user.imagenPerfil = image.ThumbnailSrc;
  }

  ionViewDidLoad() {
    this.usuario.get().subscribe(data => {
      this.user = {
        id: this.usuario.getUserId(),
        nombre: data.nombre,
        apellido: data.apellido,
        email: data.email,
        password: data.password,
        imagenPerfil: data.imagenPerfil,
        fechaNacimiento: data.fechaNacimiento
      }
    });
  }

  update() {
    this.usuario.put(this.user)
      .subscribe(data => {
        this.usuario.updateFullName(this.user.nombre, this.user.apellido);
        this.toast.show("El usuario se actualizo correctamente");
      },
        response => {
          this.toast.show(!!response ?
            response.error.Email[0] :
            'No se pudo registrar, intente nuevamente');
        });
  }

  setBirthDate(date: string) {
    this.user.fechaNacimiento = date;
  }
}