import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginService } from '../../services/login.service';
import { RegistrarsePage } from '../registrarse/registrarse';
import { ToastService } from '../../services/toast.service';
import { DestacadosPage } from '../destacados/destacados';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IUsuarioLogin } from '../../interfaces/IUsuarioLogin';
import { UsuariosService } from '../../services/usuarios.service';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { IFacebook } from '../../interfaces/IFacebook';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  form: FormGroup;
  facebook: IFacebook = {
    acces_token: ""
  };
  constructor(public navCtrl: NavController,
    private loginService: LoginService,
    private usuariosService: UsuariosService,
    private nav: NavController,
    private toast: ToastService,
    public formBuilder: FormBuilder,
    private fb: Facebook,
    private toastService: ToastService) {
    this.validacionesFormularioLogin(formBuilder);
  }

  validacionesFormularioLogin(formulario: FormBuilder) {
    this.form = formulario.group({
      email: ['', [Validators.email, Validators.required]],
      password: ['', Validators.required],
    });
  }

  loginWithFacebook(){
    
   this.fb.login(['public_profile', 'email'])
   .then((res: FacebookLoginResponse) => {
    console.log("Debajo esta el status");
    console.log(res.status);
    if(res.status == "connected"){
       console.log(res.authResponse.accessToken);
       this.toastService.show(res.authResponse.accessToken);
       this.facebook.acces_token = res.authResponse.accessToken;
       this.loginService.postFacebook(this.facebook).subscribe(resFb => {
          this.loginService.login(resFb);
          console.log(resFb);
          if (this.loginService.isLoggedIn) {
            this.nav.setRoot(DestacadosPage);
          } 
       },
       err => {
         console.log("fallo el suscribe" + err);
       });
     }
    })
   .catch(e => console.log('Error logging into Facebook', e));
   }

  login() {
    let user = <IUsuarioLogin>this.form.value;
    this.loginService.post(user)
      .subscribe(data => {
        this.loginService.login(data);
        this.usuariosService.saveFullName();
        if (this.loginService.isLoggedIn) {
          this.nav.setRoot(DestacadosPage);
        } 
      });
  }

  goToRegistrarse() {
    this.navCtrl.push(RegistrarsePage);
  }
}