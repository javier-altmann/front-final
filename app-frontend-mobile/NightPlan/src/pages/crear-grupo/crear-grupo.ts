import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Usuarios } from '../../interfaces/usuarios';
import { UsuariosVm } from '../../interfaces/usuariosVm';
import { UsuariosService } from '../../services/usuarios.service';
import { GruposService } from '../../services/grupos.service';
import { GruposDelUsuarioPage } from '../gruposdelusuario/gruposdelusuario';
import { ToastService } from '../../services/toast.service';
import { ImageResource } from '../../services/image-picker.service';

@Component({
  selector: 'page-crear-grupo',
  templateUrl: 'crear-grupo.html',
})
export class CrearGrupoPage {
  usuarios: Usuarios[] = [];
  nombreDelGrupo: string;
  usuariosMails: string[] = [];
  selectedPicture: ImageResource;

  constructor(
    public navCtrl: NavController,
    private gruposService: GruposService,
    private usuariosService: UsuariosService,
    private toastService: ToastService) { }

  imageSelected(image: ImageResource) {
    this.selectedPicture = image;
  }
  create() {
    this.gruposService.post(
      {
        nombre: this.nombreDelGrupo,
        emails: this.usuarios,
        Imagen: this.selectedPicture ? this.selectedPicture.ThumbnailSrc : null,
      }).subscribe(x => {
        this.navCtrl.popTo(GruposDelUsuarioPage);
        this.toastService.show('Grupo creado exitosamente');
      });
  }

  onKeyUp(event) {
    let query = event.target.value;
    if (query.length < 3) {
      return;
    }
    this.usuariosService.Query(query, false).subscribe((usuarios: UsuariosVm[]) =>
      this.usuariosMails = usuarios.map(x => x.email));
  }
}
