import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { EstablecimientosService } from '../../services/establecimientos.service';
import { IEstablecimientoConDetalle } from '../../interfaces/IEstablecimientoConDetalle';


@Component({
  selector: 'page-detalle-establecimiento',
  templateUrl: 'detalle-establecimiento.html',
})
export class DetalleEstablecimientoPage {
  establecimientoId: number;
  establecimiento: IEstablecimientoConDetalle;

  constructor(navParams: NavParams,
    private establecimientosService: EstablecimientosService,
    private navCtrl: NavController) {
    this.establecimientoId = navParams.get('detalleId');
    this.getEstablecimientosConDetalleSubscribe();
  }

  private getEstablecimientosConDetalleSubscribe(){
    this.establecimientosService.get(this.establecimientoId)
    .subscribe((response: IEstablecimientoConDetalle) => {
      this.establecimiento = response;
    });
  }

  back(){
    this.navCtrl.pop();
  }
}
