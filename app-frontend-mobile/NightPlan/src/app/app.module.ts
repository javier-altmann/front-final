import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import { MyApp } from './app.component';
import { Facebook } from '@ionic-native/facebook';
import { DestacadosPage } from '../pages/destacados/destacados';
import { GrupoPage } from '../pages/grupo/grupo';
import { GruposDelUsuarioPage } from '../pages/gruposdelusuario/gruposdelusuario';
import { LoginPage } from '../pages/login/login';
import { RegistrarsePage } from '../pages/registrarse/registrarse';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { PreferenciasPage } from '../pages/preferencias/preferencias';
import { CrearGrupoPage } from '../pages/crear-grupo/crear-grupo';
import { MiCuentaPage } from '../pages/mi-cuenta/mi-cuenta';
import { HttpClientModule } from '@angular/common/http';
import { LoginService } from '../services/login.service';
import { RegistrarseService } from '../services/registrarse.service';
import { EstablecimientosService } from '../services/establecimientos.service';
import { Toast } from '@ionic-native/toast';
import { GruposService } from '../services/grupos.service';
import { IonicStorageModule } from '@ionic/storage';
import { RlTagInputModule } from "angular2-tag-input";
import { PreferenciasService } from '../services/preferencias.service';
import { ConfigService } from '../services/config.service';
import { UsuariosService } from '../services/usuarios.service';
import { ToastService } from '../services/toast.service';
import { ApiClient } from '../services/api-client.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { Camera } from '@ionic-native/camera';
import { ImagePickerService } from '../services/image-picker.service';
import { ImageUploaderComponent } from '../components/image-uploader/image-uploader';
import { DetalleEstablecimientoPage } from '../pages/detalle-establecimiento/detalle-establecimiento';
import { VotacionService } from '../services/votacion.service';

@NgModule({
  declarations: [
    MyApp,
    DestacadosPage,
    GrupoPage,
    GruposDelUsuarioPage,
    LoginPage,
    RegistrarsePage,
    PreferenciasPage,
    CrearGrupoPage,
    MiCuentaPage,
    DetalleEstablecimientoPage,
    ImageUploaderComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RlTagInputModule,
    FontAwesomeModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    DestacadosPage,
    GrupoPage,
    GruposDelUsuarioPage,
    LoginPage,
    RegistrarsePage,
    PreferenciasPage,
    CrearGrupoPage,
    DetalleEstablecimientoPage,
    MiCuentaPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    LoginService,
    RegistrarseService,
    EstablecimientosService,
    GruposService,
    PreferenciasService,
    Toast,
    ConfigService,
    UsuariosService,
    ToastService,
    VotacionService,
    ApiClient,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Facebook,
    Camera,
    ImagePickerService
  ]
})
export class AppModule {}
