import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, App, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { MiCuentaPage } from '../pages/mi-cuenta/mi-cuenta';
import { DestacadosPage } from '../pages/destacados/destacados';
import { GruposDelUsuarioPage } from '../pages/gruposdelusuario/gruposdelusuario';
import { LoginService } from '../services/login.service';
import { UsuariosService } from '../services/usuarios.service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  name: String;
  pages: Array<{ key: string, title: string, component: any, icon: string }>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public app: App,
    public alertCtrl: AlertController,
    private loginService: LoginService,
    private usuariosService: UsuariosService) {
    // used for an example of ngFor and navigation
    this.pages = [
      { key: 'destacados', title: 'Destacados', component: DestacadosPage, icon: "md-star" },
      { key: 'grupos', title: 'Grupos', component: GruposDelUsuarioPage, icon: "md-contacts" },
      { key: 'miCuenta', title: 'Mi cuenta', component: MiCuentaPage, icon: "md-contact" }
    ];

    this.initializeApp();
  }

  get fullName() {
    return this.usuariosService.fullName;
  }

  get rootPage(): any {
    return this.loginService.isLoggedIn ?
      DestacadosPage
      : LoginPage;
  }

  getPage(key: string): any {
    return this.pages.find(x => x.key === key);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    this.platform.registerBackButtonAction(() => {
      let nav = this.app.getActiveNav();
      let activeView = nav.getActive();
      if (nav.canGoBack()) {
        nav.pop();
      } else if (activeView.name === 'DestacadosPage' || activeView.name === 'LoginPage') {
        this.modalCloseApp();
      }
      else {
        this.nav.setRoot(DestacadosPage);
      }
    });
  }
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logout(event) {
    this.loginService.logout();
    this.nav.setRoot(LoginPage);
  }

  modalCloseApp() {
    const alert = this.alertCtrl.create({
      title: 'Cerrar NightPlan',
      message: '¿Está seguro que desea cerrar la aplicación?',
      buttons: [{
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {
        }
      }, {
        text: 'Cerrar NightPlan',
        handler: () => {
          this.platform.exitApp();
        }
      }]
    });
    alert.present();
  }
}