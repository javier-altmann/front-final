import { IUsuario } from "./IUsuario";

export interface IUsuarioConResultado extends IUsuario {
  resultado: boolean;
}
