export interface IGrupo {
    id: number;
    nombre: string;
    imagenPerfil: string;
    fechaCreacion: string;
}