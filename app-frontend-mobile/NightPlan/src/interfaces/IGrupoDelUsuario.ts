import { IGrupo } from "./IGrupo";

export interface IGrupoDelUsuario {
    data: IGrupo[];
    message: string;
}