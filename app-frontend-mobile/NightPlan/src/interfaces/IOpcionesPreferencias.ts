export interface IOpcionesPreferencias {
    id: number;
    nombre: string;
    selected: boolean;
}