export interface IUsuario {
    id: number;
    nombre: string;
    apellido: string;
    imagenPerfil: string;
}