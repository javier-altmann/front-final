export interface IUsuarioRegistrado {
    id: number;
    nombre: string;
    apellido: string;
    email: string;
    password: string;
    fechaNacimiento: string;
    imagenPerfil: string;
}