export interface IPreferenciasSelected {
    IdsBarrios: number[];
    IdsGastronomia: number[];
    IdsCaracteristicas: number[];
}