export interface IResultadoVotacion {
    name: String;
    idEstablecimiento: number,
    cantidadDeVotos: number,
    gano: boolean
}