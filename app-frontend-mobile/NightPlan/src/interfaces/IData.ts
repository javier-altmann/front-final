import { IResultadoVotacion } from "./IResultadoVotacion";

export interface IData {
    data: IResultadoVotacion;
}