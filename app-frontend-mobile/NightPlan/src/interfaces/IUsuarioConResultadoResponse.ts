import { IUsuarioConResultado } from "./IUsuarioConResultado";

export interface IUsuarioConResultadoResponse {
    respuestaLoguedUser: IUsuarioConResultado;
    respuestas: IUsuarioConResultado[]
}