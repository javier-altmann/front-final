export interface IEstablecimiento {
    id: number;
    nombre: string;
    imagen: string;
    direccion: string;
    destacado: boolean;
}
