export interface ILoginResult {
  token: string;
  id: string;
}
