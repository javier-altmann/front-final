import { IPreferenciasSelected } from "./IPreferenciasSelected";

export interface IPreferencias {
    grupoId: number;
    respuesta: IPreferenciasSelected;
}
