import { IEstablecimiento } from "./IEstablecimiento";

export interface IEstablecimientoConDetalle extends IEstablecimiento {
    caracteristicas: string[];
    gastronomias: string[];
}
