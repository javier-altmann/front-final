import { IEstablecimiento } from "./IEstablecimiento";

export interface IEstablecimientoDestacado {
    data: IEstablecimiento[];
    message: string;
}